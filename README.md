<!-- AIR:tour -->

# Sonic Test Drive: Lab Guide, Lab 1

## Lab 1: Verifying Lab Connectivity

**Objective:**
You will confirm that you can access your NVIDIA Cumulus In the Cloud (CITC) workbench topology. This involves first visiting your Cumulus In The Cloud (CITC) workbench in a web browser. You will connect to your Lab workbench via SSH to an out-of-band management server (oob-mgmt-server), from which you can access your switches.

**Goals:**

- Learn how to access your CITC workbench in a web browser
- Log into your oob-mgmt-server.
- From your oob-mgmt-server, access your switches via SSH.
- View the running containers in SONiC

**Procedure:**
To access your lab you will need to be registered with cumulusnetworks.com. You must have an account with cumulusnetworks.com that is bound to the email address used for your workshop registration.

##

##Access your CITC workbench

1. Use a web browser to access and log into https://air.nvidia.com

2. Once at the NVIDIA Air console, find your SONiC Virtual Test Drive simulation

3. Click the “Launch” button to open your simulation console

##

##Connect to your oob-mgmt-server

1. This view presents you with your console connection to the oob-mgmt-server. Click on the pop-out icon to pop out your oob-mgmt-server console to allow you to resize and position for your convenience.

2. You can also click on any of the nodes in the “Nodes” list to pop out a console window to that device:

3. Log into the oob-mgmt-server. You will be asked to change your password on your first login to a new, unique password. First, login with the credentials according to the pre-login banner:

```
Username: cumulus
Password: CumulusLinux!
```

Then, follow the instructions to set a new password.

Run the setup playbook

1. Change directories to the folder named “automation” from the user cumulus home directory.

```
cumulus@oob-mgmt-server:~$ git clone https://gitlab.com/cumulus-consulting/education/sonic-testdrive
cumulus@oob-mgmt-server:~$ cd sonic-testdrive/automation
cumulus@oob-mgmt-server:~/sonic-testdrive/automation$
```

2. Run the ‘sonic-setup.yml’ Ansible playbook.

```
cumulus@oob-mgmt-server:~/sonic-testdrive/automation$ ansible-playbook sonic-setup.yml

PLAY [servers] ******************************************\*******************************************

TASK [Gathering Facts] **************************************\***************************************
Saturday 19 June 2021 20:39:27 +0000 (0:00:00.064) 0:00:00.064 ****\*****
ok: [server03]
ok: [server01]
ok: [server02]

TASK [servers : Verify Minimum Software Version - Ubuntu 18.04] ****************\*\*\*\*****************
Saturday 19 June 2021 20:39:31 +0000 (0:00:04.279) 0:00:04.344 ****\*****
ok: [server01] => changed=false
msg: All assertions passed
ok: [server02] => changed=false
msg: All assertions passed
ok: [server03] => changed=false
msg: All assertions passed

TASK [servers : Run the equivalent of "apt-get update"] ********************\*\*\*\*********************
Saturday 19 June 2021 20:39:32 +0000 (0:00:00.528) 0:00:04.873 ****\*****
changed: [server01]
changed: [server02]
changed: [server03]

TASK [servers : Copy interfaces configuration] **************************\***************************
Saturday 19 June 2021 20:39:45 +0000 (0:00:13.090) 0:00:17.964 ****\*****
ok: [server02]
ok: [server03]
ok: [server01]

TASK [servers : Install traceroute] ******************************\*\*\*\*******************************
Saturday 19 June 2021 20:39:49 +0000 (0:00:04.205) 0:00:22.169 ****\*****
ok: [server01]
ok: [server02]
ok: [server03]

PLAY [sonicswitches] **************************************\*\*\***************************************

TASK [Gathering Facts] **************************************\***************************************
Saturday 19 June 2021 20:39:55 +0000 (0:00:05.484) 0:00:27.654 ****\*****
ok: [spine01]
ok: [leaf01]
ok: [leaf02]

TASK [sonic-setup : Create cumulus user] ****************************\*\*\*****************************
Saturday 19 June 2021 20:40:05 +0000 (0:00:10.470) 0:00:38.124 ****\*****
ok: [spine01]
ok: [leaf01]
ok: [leaf02]

TASK [sonic-setup : Create an ssh directory if it does not exist] ****************\*\*****************
Saturday 19 June 2021 20:40:07 +0000 (0:00:02.115) 0:00:40.240 ****\*****
ok: [spine01]
ok: [leaf01]
ok: [leaf02]

TASK [sonic-setup : Download cumulus User Key] **************************\***************************
Saturday 19 June 2021 20:40:09 +0000 (0:00:01.586) 0:00:41.826 ****\*****
ok: [spine01]
ok: [leaf02]
ok: [leaf01]

TASK [sonic-setup : Restore config_db] ******************************\*******************************
Saturday 19 June 2021 20:40:10 +0000 (0:00:01.792) 0:00:43.618 ****\*****
changed: [spine01]
changed: [leaf02]
changed: [leaf01]

RUNNING HANDLER [sonic-setup : reload sonic config] **********************\*\*\*\***********************
Saturday 19 June 2021 20:40:13 +0000 (0:00:02.594) 0:00:46.213 ****\*****
changed: [spine01]
changed: [leaf02]
changed: [leaf01]

PLAY RECAP ********************************************\*********************************************
leaf01 : ok=6 changed=2 unreachable=0 failed=0 skipped=0 rescued=0 ignored=0
leaf02 : ok=6 changed=2 unreachable=0 failed=0 skipped=0 rescued=0 ignored=0
server01 : ok=5 changed=1 unreachable=0 failed=0 skipped=0 rescued=0 ignored=0
server02 : ok=5 changed=1 unreachable=0 failed=0 skipped=0 rescued=0 ignored=0
server03 : ok=5 changed=1 unreachable=0 failed=0 skipped=0 rescued=0 ignored=0
spine01 : ok=6 changed=2 unreachable=0 failed=0 skipped=0 rescued=0 ignored=0

# Saturday 19 June 2021 20:40:47 +0000 (0:00:33.964) 0:01:20.177 ****\*****

sonic-setup : reload sonic config ---------------------------------------------------------- 33.97s
servers : Run the equivalent of "apt-get update" ------------------------------------------- 13.09s
Gathering Facts ---------------------------------------------------------------------------- 10.47s
servers : Install traceroute ---------------------------------------------------------------- 5.48s
Gathering Facts ----------------------------------------------------------------------------- 4.28s
servers : Copy interfaces configuration ----------------------------------------------------- 4.21s
sonic-setup : Restore config_db ------------------------------------------------------------- 2.59s
sonic-setup : Create cumulus user ----------------------------------------------------------- 2.12s
sonic-setup : Download cumulus User Key ----------------------------------------------------- 1.79s
sonic-setup : Create an ssh directory if it does not exist ---------------------------------- 1.59s
servers : Verify Minimum Software Version - Ubuntu 18.04 ------------------------------------ 0.53s
```

Verify the SONiC Version 4. On oob-mgmt-server: SSH to the SONiC leaf01 with the following command: “ssh leaf01”

```
cumulus@oob-mgmt-server:~/sonic-testdrive/automation$ ssh leaf01
Linux leaf01 4.19.0-12-2-amd64 #1 SMP Debian 4.19.152-1 (2020-10-18) x86_64
You are on

---

/ **_| / _ \| \ | (_)/ _**|
\_** \| | | | \| | | |
\_**) | |\_| | |\ | | |**\_
|\_\_**/ \_**/|_| \_|_|\_\_**|

-- Software for Open Networking in the Cloud --

Unauthorized access and/or use are prohibited.
All access and/or use are subject to monitoring.

Help: http://azure.github.io/SONiC/

---

Running DEBUG image

---

/src has the sources
/src is mounted in each docker
/debug is created for core files or temp files
Create a subdir under /debug to upload your files
/debug is mounted in each docker
Last login: Fri Jun 18 03:36:33 2021 from 192.168.200.1
cumulus@leaf01:~$
```

5. On leaf01: Run the “show version” command from the CLI to verify the running version of SONiC code on leaf01.

```
cumulus@leaf01:~$ show version

SONiC Software Version: SONiC.202012.63-5bdbfcfb
Distribution: Debian 10.9
Kernel: 4.19.0-12-2-amd64
Build commit: 5bdbfcfb
Build date: Sat Apr 17 10:06:01 UTC 2021
Built by: johnar@jenkins-worker-8

Platform: x86_64-kvm_x86_64-r0
HwSKU: Force10-S6000
ASIC: vs
ASIC Count: 1
Serial Number: 000000
Uptime: 20:42:35 up 12:25, 1 user, load average: 26.79, 15.06, 9.45
```

6. On leaf01: Verify the running containers on the SONiC switch with the “sudo docker ps” command

```
cumulus@leaf01:~$ sudo docker ps
CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES
c9b64a826671 docker-sonic-mgmt-framework-dbg:latest "/usr/local/bin/supe…" 45 hours ago Up About a minute mgmt-framework
e080e29d05ac docker-snmp-dbg:latest "/usr/local/bin/supe…" 45 hours ago Up About a minute snmp
da57b3f6c384 docker-sonic-telemetry-dbg:latest "/usr/local/bin/supe…" 45 hours ago Up About a minute telemetry
0b08bc9c030e docker-router-advertiser-dbg:latest "/usr/bin/docker-ini…" 8 weeks ago Up About a minute radv
a9ab801c2be0 docker-lldp-dbg:latest "/usr/bin/docker-lld…" 8 weeks ago Up About a minute lldp
5933cfdabf4e docker-dhcp-relay-dbg:latest "/usr/bin/docker_ini…" 8 weeks ago Up About a minute dhcp_relay
c17c20892b4b docker-syncd-vs-dbg:latest "/usr/local/bin/supe…" 8 weeks ago Up About a minute syncd
2309a544e23d docker-gbsyncd-vs-dbg:latest "/usr/local/bin/supe…" 8 weeks ago Up About a minute gbsyncd
c9892b559d6e docker-teamd-dbg:latest "/usr/local/bin/supe…" 8 weeks ago Up About a minute teamd
e2a1bba5c70b docker-platform-monitor-dbg:latest "/usr/bin/docker_ini…" 8 weeks ago Up About a minute pmon
91a572048d90 docker-orchagent-dbg:latest "/usr/bin/docker-ini…" 8 weeks ago Up About a minute swss
c5b97f720744 docker-fpm-frr-dbg:latest "/usr/bin/docker_ini…" 2 months ago Up 2 minutes bgp
f8ecda270feb docker-database-dbg:latest "/usr/local/bin/dock…" 2 months ago Up 45 hours database
```

This concludes Lab 1.

<!-- AIR:tour -->
